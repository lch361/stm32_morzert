#![no_std]
#![no_main]
#![allow(unused)]
#![feature(const_trait_impl)]
#![feature(const_fn_floating_point_arithmetic)]

use core::convert::TryInto;

mod cortex_m3;
mod extra;
mod stm32;

mod ringbuffer;
mod morze;

use ringbuffer::Ringbuffer;
use morze::MorzeCode;

use extra::io::Write;
use stm32::{
    gpio::{self, GPIOC, GPIOA},
    rcc::{self, RCC},
    usart::{self, USART1},
    exti::EXTI,
    tim::{general::TIM2, self}
};

use cortex_m3::{
    systick::{self, STK},
    scb::{self, SCB},
    nvic::{self, NVIC}
};

#[repr(u8)]
#[derive(Clone, Copy)]
enum ButtonState { WaitChar, WaitSpace, GetSignal }

struct Signal {
    duration: u32,
    last_tick: u32,
}

impl Signal {
    pub const fn new() -> Self {
        Self { last_tick: 0, duration: 0 }
    }

    /// Returns delta
    pub fn update(&mut self) -> u32 {
        let tick = unsafe { TICK };
        let interval = tick - self.last_tick;
        self.last_tick = tick;
        self.duration += interval;
        interval
    }

    pub fn duration(&self) -> u32 {
        self.duration
    }

    pub fn clear(&mut self) {
        self.duration = 0;
    }
}

/// Button pin
const BUTTON_PIN: u8 = 0;
const BUTTON_PIN_VECTOR: stm32::boot::Vector = stm32::boot::Vector::EXTI0;

/// LED which lights on button press
const LED_BUTTON_PIN: u8 = 1;

/// LED which indicates decoded value
const LED_OUTPUT_PIN: u8 = 2;

const EXTI_PLAY_VECTOR: stm32::boot::Vector = stm32::boot::Vector::EXTI1;
const EXTI_SEND_VECTOR: stm32::boot::Vector = stm32::boot::Vector::EXTI2;

const EXTI_PLAY_NUM: u8 = 1;
const EXTI_SEND_NUM: u8 = 2;

const DOT_TIME: u32 = 150;
const HYPHEN_TIME: u32 = DOT_TIME * 3;
const SPACE_TIME: u32 = DOT_TIME * 14;
const CHAR_DEL_TIME: u32 = DOT_TIME * 3;
const ELEMENT_DEL_TIME: u32 = DOT_TIME;

const BOUNCE_TIME: u32 = 15;

const HSI_FREQUENCY: u32 = 8000000;

const BUFSIZE: usize = 4096;

static mut TICK: u32 = 0;

static mut RECEIVE_BUFFER: Ringbuffer<BUFSIZE> = Ringbuffer::new();
static mut SEND_BUFFER: Ringbuffer<BUFSIZE> = Ringbuffer::new();

static mut MORZE_CODE_INPUT: Option<MorzeCode> = None;
static mut SIGNAL: Signal = Signal::new();
static mut BUTTON_STATE: ButtonState = ButtonState::WaitChar;

static mut TIM2_STARTED: bool = true;

fn delay(ticks: u32) {
    let start_tick = unsafe { TICK };
    while unsafe { TICK } - start_tick <= ticks {
        cortex_m3::asm::nop();
    }
}

extern "C" fn main() {
    // Initialize statics here because compiler can't in this environment
    unsafe {
        RECEIVE_BUFFER = Ringbuffer::new();
        SEND_BUFFER = Ringbuffer::new();
        MORZE_CODE_INPUT = None;
        SIGNAL = Signal::new();
        BUTTON_STATE = ButtonState::WaitChar;
        TIM2_STARTED = true;
    }

    RCC.devices()
        .enable(rcc::device::Apb2::Iopa)
        .enable(rcc::device::Apb2::Usart1)
        .enable(rcc::device::Apb1::Tim2)
        .execute();

    GPIOA.config_write()
        .output(9, gpio::Output::AFPushPull, gpio::MaxSpeed::Mhz50)
        .input(10, gpio::Input::Floating)
        .input(BUTTON_PIN, gpio::Input::Pull)
        .output(LED_BUTTON_PIN, gpio::Output::PushPull, gpio::MaxSpeed::Mhz10)
        .output(LED_OUTPUT_PIN, gpio::Output::PushPull, gpio::MaxSpeed::Mhz10)
        .execute();

    USART1.set_baud_rate(usart::Brr::from(115200, HSI_FREQUENCY));
    USART1.config_write()
        .set(usart::Flag::ReceiverEnable)
        .set(usart::Flag::TransmitterEnable)
        .set(usart::Flag::Enable)
        .set(usart::Flag::RxneInterruptEnable)
        .set_stop_bits(usart::StopBits::StopBit1)
        .execute();

    SCB.set_priority(cortex_m3::boot::Vector::SysTick, 0);
    NVIC.set_priority(BUTTON_PIN_VECTOR, 1);
    NVIC.set_priority(stm32::boot::Vector::USART1, 2);
    NVIC.set_priority(stm32::boot::Vector::TIM2, 3);
    NVIC.set_priority(EXTI_SEND_VECTOR, 4);
    NVIC.set_priority(EXTI_PLAY_VECTOR, 5);

    NVIC.enable(stm32::boot::Vector::USART1);
    NVIC.enable(BUTTON_PIN_VECTOR);
    NVIC.enable(EXTI_PLAY_VECTOR);
    NVIC.enable(EXTI_SEND_VECTOR);
    NVIC.enable(stm32::boot::Vector::TIM2);

    EXTI.lines()
        .set(BUTTON_PIN)
        .set(EXTI_PLAY_NUM)
        .set(EXTI_SEND_NUM)
        .interrupt();

    EXTI.lines()
        .set(BUTTON_PIN)
        .rising_trigger();

    EXTI.lines()
        .set(BUTTON_PIN)
        .falling_trigger();

    STK.reload_set(HSI_FREQUENCY / 1000, true);
    STK.config_write()
        .set(systick::Config::UseAHB)
        .set(systick::Config::AssertIRQ)
        .set(systick::Config::Enabled)
        .execute();

    // Millisecond
    TIM2.set_prescaler((HSI_FREQUENCY / 1000) as u16);
    TIM2.set_reload(CHAR_DEL_TIME as u16);
    TIM2.control_write()
        .set(tim::general::ControlFlag::CounterEnable)
        .set(tim::general::ControlFlag::UpdateSourceOnlyCounter)
        .set(tim::general::ControlFlag::UpdateInterruptEnable)
        .execute();

    // SCB.set_sleep_on_exit();
    // cortex_m3::asm::wfi();

    loop { }
}

extern "C" {
    fn on_hard_fault_stub();
}

extern "C" fn on_systick() {
    unsafe { TICK += 1 };
}

extern "C" fn on_button_pin() {
    let delta = unsafe { SIGNAL.update() };

    // Anti-bounce measure
    if delta > BOUNCE_TIME {
        match unsafe { BUTTON_STATE } {
            // End getting signal, entering waiting timer
            ButtonState::GetSignal => {
                unsafe { BUTTON_STATE = ButtonState::WaitChar };

                GPIOA.pins_write()
                    .clear(LED_BUTTON_PIN)
                    .execute();

                let morze_bit = unsafe { SIGNAL.duration() } > DOT_TIME;
                unsafe {
                    MORZE_CODE_INPUT = match MORZE_CODE_INPUT {
                        None => Some(MorzeCode::new(morze_bit)),
                        Some(v) => match v.push(morze_bit) {
                            Ok(v) => Some(v),
                            Err(_) => Some(MorzeCode::new(morze_bit)),
                        },
                    }
                }

                TIM2.set_reload(CHAR_DEL_TIME as u16);
                TIM2.set_counter(0);
                TIM2.control_write()
                    .set(tim::general::ControlFlag::CounterEnable)
                    .execute();
            },
            // Begin getting signal
            _ => {
                TIM2.control_write()
                    .clear(tim::general::ControlFlag::CounterEnable)
                    .execute();

                unsafe {
                    SIGNAL.clear();
                    BUTTON_STATE = ButtonState::GetSignal;
                };

                GPIOA.pins_write()
                    .set(LED_BUTTON_PIN)
                    .execute();
            }
        }
    }

    EXTI.lines()
        .clear(BUTTON_PIN)
        .pending();
}

extern "C" fn play_received_chars() {
    while unsafe { RECEIVE_BUFFER.length() } > 0 {
        let value = unsafe { RECEIVE_BUFFER.pop() };

        match value {
            0 => (),
            v if v == ' ' as u8 || v == '\n' as u8 || v == '\t' as u8 => {
                delay(SPACE_TIME);
            },
            _ => {
                let morze_code: MorzeCode = value.into();
                for i in morze_code {
                    GPIOA.pins_write()
                        .set(LED_OUTPUT_PIN)
                        .execute();

                    delay(if i { HYPHEN_TIME } else { DOT_TIME });

                    GPIOA.pins_write()
                        .clear(LED_OUTPUT_PIN)
                        .execute();

                    delay(ELEMENT_DEL_TIME);
                }

                delay(CHAR_DEL_TIME - ELEMENT_DEL_TIME);
            },
        }
    }

    EXTI.lines()
        .clear(EXTI_PLAY_NUM)
        .pending();
}

extern "C" fn send_chars() {
    while unsafe { SEND_BUFFER.length() } > 0 {
        let value = unsafe { SEND_BUFFER.pop() };
        USART1.write_byte(value);
    }

    EXTI.lines()
        .clear(EXTI_SEND_NUM)
        .pending();
}

extern "C" fn on_tim2() {
    if unsafe { TIM2_STARTED } {
        unsafe { TIM2_STARTED = false };
    } else {
        match unsafe { BUTTON_STATE } {
            ButtonState::WaitChar => {
                unsafe {
                    if let Some(v) = MORZE_CODE_INPUT {
                        match v.try_into() {
                            Ok(v) => {
                                SEND_BUFFER.append(v);
                                EXTI.lines()
                                    .set(EXTI_SEND_NUM)
                                    .pending();
                            },
                            Err(()) => (),
                        }
                    }
                    MORZE_CODE_INPUT = None;
                    BUTTON_STATE = ButtonState::WaitSpace;
                }
                TIM2.set_reload((SPACE_TIME - CHAR_DEL_TIME) as u16);
            },
            ButtonState::WaitSpace => {
                unsafe { SEND_BUFFER.append(b' ') };

                EXTI.lines()
                    .set(EXTI_SEND_NUM)
                    .pending();

                TIM2.set_reload(SPACE_TIME as u16);
            },
            _ => (),
        }
    }

    TIM2.status_write()
        .clear(tim::general::Status::UpdateInterruptPending)
        .execute();
}

extern "C" fn on_usart1() {
    let received = unsafe { USART1.raw_read_data() };
    unsafe { RECEIVE_BUFFER.append(received) };

    EXTI.lines()
        .set(EXTI_PLAY_NUM)
        .pending();
}

#[link_section = ".boot"]
#[used]
static BOOT_SECTOR: cortex_m3::boot::Sector = cortex_m3::boot::Sector::new(0x20002800)
    .add(cortex_m3::boot::Vector::Reset, main)
    .add(cortex_m3::boot::Vector::HardFault, on_hard_fault_stub)
    .add(cortex_m3::boot::Vector::SysTick, on_systick)
    .add(BUTTON_PIN_VECTOR, on_button_pin)
    .add(EXTI_PLAY_VECTOR, play_received_chars)
    .add(EXTI_SEND_VECTOR, send_chars)
    .add(stm32::boot::Vector::TIM2, on_tim2)
    .add(stm32::boot::Vector::USART1, on_usart1);

#[link_section = ".useless"]
#[panic_handler]
fn panic_handler(_info: &core::panic::PanicInfo) -> ! {
    loop {}
}
