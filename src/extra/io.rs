pub trait Read {
    fn read_byte(&self) -> u8;
    fn read(&self, destination: &mut [u8]) {
        for i in destination {
            *i = self.read_byte();
        }
    }
}

pub trait Write {
    fn write_byte(&self, source: u8);

    /// This is a blocking operation
    fn write(&self, source: &[u8]) {
        for i in source {
            self.write_byte(*i);
        }
    }
}
