pub const fn floor(n: f32) -> u32 {
    n as u32
}

pub const fn frac(n: f32) -> f32 {
    n - (floor(n) as f32)
}

pub const fn round(n: f32) -> u32 {
    floor(n) + (frac(n) >= 0.5_f32) as u32
}
