pub unsafe fn modify_volatile<T, F>(address: *mut T, action: F)
where
    F: Fn(T) -> T,
{
    let val = core::ptr::read_volatile(address);
    core::ptr::write_volatile(address, action(val));
}
