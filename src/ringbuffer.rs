pub struct Ringbuffer<const S: usize> {
    buf: [u8; S],
    start: usize,
    end: usize,
    length: usize
}

impl<const S: usize> Ringbuffer<S> {
    pub const fn new() -> Self {
        Self { buf: [0; S], start: 0, end: 0, length: 0 }
    }

    /// If the buffer is filled to the brim, nothing happens
    pub fn append(&mut self, byte: u8) {
        if (self.length == S) {
            return;
        }

        unsafe {
            *self.buf.get_unchecked_mut(self.end) = byte;
        }
        self.end = (self.end + 1) % S;
        self.length += 1;
    }

    /// Please don't call this function when you have 0 length buffer
    pub unsafe fn pop(&mut self) -> u8 {
        let byte = unsafe { *self.buf.get_unchecked(self.start) };
        self.start = (self.start + 1) % S;
        self.length -= 1;
        byte
    }

    pub fn length(&self) -> usize {
        self.length
    }
}
