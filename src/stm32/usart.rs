use crate::extra::{float::*, io::*, ptr::modify_volatile};

const SR_OFFSET: u32 = 0x00;
const DR_OFFSET: u32 = 0x04;
const BRR_OFFSET: u32 = 0x08;
const CR1_OFFSET: u32 = 0x0C;
const CR2_OFFSET: u32 = CR1_OFFSET + 0x4;
const CR3_OFFSET: u32 = CR2_OFFSET + 0x4;
const GTPR_OFFSET: u32 = 0x18;

#[repr(u8)]
pub enum Flag {
    SendBreak,
    ReceiverWakeup,
    ReceiverEnable,
    TransmitterEnable,
    IdleEnable,
    RxneInterruptEnable,
    TransmissionCompleteInterruptEnable,
    TxeInterruptEnable,
    PeInterruptEnable,
    ParitySelection,
    ParityControlEnable,
    WakeupMethodAddressMark,
    WordLength9,
    Enable,
    LinBreakLength11 = 37,
    LibBreakInterruptEnable,
    LastBitClockPulseIsClockOutput = 40,
    ClockPhaseSecond,
    ClockPolarityHigh,
    ClockEnable,
    LinModeEnable = 46,
    ErrorInterruptEnable = 64,
    IrDaModeEnable,
    IrDaLowPower,
    HalfDuplexSelection,
    SmartcardNackEnable,
    SmartcardModeEnable,
    DmaEnableReceiver,
    DmaEnableTransmitter,
    RtsEnable,
    CtsEnable,
    CtsInterruptEnable,
}

#[repr(u8)]
pub enum StopBits { StopBit1, StopBit0_5, StopBit2, StopBit1_5 }

#[repr(u8)]
pub enum Status {
    ParityError, FramingError, NoiseError, OverrunError, IDLEdetected, ReadNotEmpty,
    TransmissionComplete, TransmitEmpty, LINdetected, CTSchange
}

pub struct Brr { value: u32 }

impl Brr {
    /// From this equation:
    /// baud_rate = clock_frequency / (16 * USARTDIV)
    /// First evaluates USARTDIV,
    /// Then transforms it into mantissa/fraction form for USART to understand
    pub const fn from(baud_rate: u32, clock_frequency: u32) -> Self {
        let usartdiv = (clock_frequency as f32) / (16_f32 * baud_rate as f32);
        let mantissa = floor(usartdiv);
        let fraction = round(frac(usartdiv) * 16_f32);
        Brr { value: fraction | (mantissa << 4) }
    }
}

pub struct UsartConfig<'a> {
    usart: &'a Usart,
    cr1_value: u32,
    cr2_value: u32,
    cr3_value: u32,
    cr1_mask: u32,
    cr2_mask: u32,
    cr3_mask: u32,
}

impl UsartConfig<'_> {
    pub fn set(mut self, flag: Flag) -> Self {
        let num = flag as u8;
        if num < 32 {
            self.cr1_value |= 1 << num;
        } else if num < 64 {
            self.cr2_value |= 1 << (num - 32);
        } else {
            self.cr3_value |= 1 << (num - 64);
        }
        self
    }

    pub fn clear(mut self, flag: Flag) -> Self {
        let num = flag as u8;
        if num < 32 {
            self.cr1_mask |= 1 << num;
        } else if num < 64 {
            self.cr2_mask |= 1 << (num - 32);
        } else {
            self.cr3_mask |= 1 << (num - 64);
        }
        self
    }

    pub fn set_stop_bits(mut self, value: StopBits) -> Self {
        let stop_num = value as u8;
        self.cr2_mask  |= 0x3 << 12;
        self.cr2_value |= (stop_num as u32) << 12;
        self
    }

    pub fn execute(self) {
        macro_rules! modify {
            ($offset:expr, $closure:expr) => {
                let address = (self.usart.base_address + $offset) as *mut u32;
                unsafe {
                    modify_volatile(address, $closure);
                }
            };
        }

        if self.cr2_value != 0 || self.cr2_mask != 0 {
            modify!(CR2_OFFSET, |x| x & !self.cr2_mask | self.cr2_value);
        }

        if self.cr1_value != 0 || self.cr1_mask != 0 {
            modify!(CR1_OFFSET, |x| x & !self.cr1_mask | self.cr1_value);
        }

        if self.cr3_value != 0 || self.cr3_mask != 0 {
            modify!(CR3_OFFSET, |x| x & !self.cr3_mask | self.cr3_value);
        }
    }
}

pub struct UsartStatus<'a> {
    usart: &'a Usart,
    yes: u32,
    no: u32,
}

impl UsartStatus<'_> {
    pub fn yes(mut self, value: Status) -> Self {
        self.yes |= 1 << (value as u8);
        self
    }

    pub fn no(mut self, value: Status) -> Self {
        self.no |= 1 << (value as u8);
        self
    }

    pub fn answer(self) -> bool {
        let address = (self.usart.base_address + SR_OFFSET) as *const u32;
        let value = unsafe { core::ptr::read_volatile(address) };
        (value & self.yes == self.yes) && (value & self.no == 0)
    }
}

pub struct Usart {
    base_address: u32,
}

impl Usart {
    pub fn set_baud_rate(&self, brr: Brr) {
        let address = (self.base_address + BRR_OFFSET) as *mut u32;
        unsafe {
            core::ptr::write_volatile(address, brr.value);
        }
    }

    pub fn status(&self) -> UsartStatus {
        UsartStatus { usart: self, yes: 0, no: 0 }
    }

    pub fn config_write(&self) -> UsartConfig {
        UsartConfig { usart: self, cr1_value: 0, cr2_value: 0, cr3_value: 0,
            cr1_mask: 0, cr2_mask: 0, cr3_mask: 0 }
    }

    /// This function simply reads the DR register of USART, no matter what it contains
    /// # Safety
    /// Use this function only when you know exactly that Status::ReadNotEmpty is set.
    /// For example, using it in USART Rxne interrupt right at the start is safe.
    /// Otherwise, consider USART.read_byte()
    pub unsafe fn raw_read_data(&self) -> u8 {
        let address = (self.base_address + DR_OFFSET) as *const u32;
        (unsafe { core::ptr::read_volatile(address) } & 0xFF) as u8
    }
}

impl Read for Usart {
    fn read_byte(&self) -> u8 {
        while self.status().no(Status::ReadNotEmpty).answer() { }
        let address = (self.base_address + DR_OFFSET) as *const u32;
        let dr = unsafe { core::ptr::read_volatile(address) };
        (dr & 0xFF) as u8
    }
}

impl Write for Usart {
    fn write_byte(&self, source: u8) {
        let address = (self.base_address + DR_OFFSET) as *mut u32;
        unsafe { core::ptr::write_volatile(address, source as u32) };
        while self.status().no(Status::TransmissionComplete).answer() { }
    }
}

pub const USART1: Usart = Usart { base_address: 0x40013800 };
pub const USART2: Usart = Usart { base_address: 0x40004400 };
pub const USART3: Usart = Usart { base_address: 0x40004800 };
