use core::ptr::write_volatile;
use crate::extra::ptr::modify_volatile;

const CR1_OFFSET: u32 = 0x00;
const CR2_OFFSET: u32 = 0x04;
const DIER_OFFSET: u32 = 0x0C;
const CNT_OFFSET: u32 = 0x24;
const PSC_OFFSET: u32 = 0x28;
const ARR_OFFSET: u32 = 0x2C;
const SR_OFFSET: u32 = 0x10;

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum ControlFlag {
    CounterEnable,
    UpdateEventDisable,
    UpdateSourceOnlyCounter,
    OnePulseMode,
    Downcount,
    AutoReloadBuffered = 7,
    DmaRequestOnUpdate = 35,
    Ti1Connected = 39,
    UpdateInterruptEnable = 64,
    CC1InterruptEnable,
    CC2InterruptEnable,
    CC3InterruptEnable,
    CC4InterruptEnable,
    TriggerInterruptEnable = 70,
    UpdateDmaRequestEnable = 72,
    CC1DmaRequestEnable,
    CC2DmaRequestEnable,
    CC3DmaRequestEnable,
    CC4DmaRequestEnable,
    TriggerDmaRequestEnable = 78,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum AlignMode {
    EdgeAligned,
    CenterAlignedInterruptOnDown,
    CenterAlignedInterruptOnUp,
    CenterAlignedInterruptOnBoth,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum MasterMode {
    Reset,
    Enable,
    Update,
    ComparePulse,
    CompareOc1Ref,
    CompareOc2Ref,
    CompareOc3Ref,
    CompareOc4Ref,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum ClockDivision {
    Dts1CkInt,
    Dts2CkInt,
    Dts4CkInt,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Status {
    UpdateInterruptPending,
    TriggerInterruptPending = 6,
}

pub struct ControlWriter<'a> {
    tim: &'a Tim,
    /// Set, clear and toggle
    cr1: (u32, u32, u32),
    /// Set, clear and toggle
    cr2: (u32, u32, u32),
    dier: (u32, u32, u32),
}

impl ControlWriter<'_> {
    pub fn set(mut self, flag: ControlFlag) -> Self {
        let bit = flag as u8;
        if bit >= 64 {
            self.dier.0 |= 1 << bit;
        } else if bit >= 32 {
            self.cr2.0 |= 1 << bit;
        } else {
            self.cr1.0 |= 1 << bit;
        }
        self
    }

    pub fn clear(mut self, flag: ControlFlag) -> Self {
        let bit = flag as u8;
        if bit >= 64 {
            self.dier.1 |= 1 << bit;
        } else if bit >= 32 {
            self.cr2.1 |= 1 << bit;
        } else {
            self.cr1.1 |= 1 << bit;
        }
        self
    }

    pub fn toggle(mut self, flag: ControlFlag) -> Self {
        let bit = flag as u8;
        if bit >= 64 {
            self.dier.2 |= 1 << bit;
        } else if bit >= 32 {
            self.cr2.2 |= 1 << bit;
        } else {
            self.cr1.2 |= 1 << bit;
        }
        self
    }

    pub fn set_align_mode(mut self, value: AlignMode) -> Self {
        let bits = (value as u32) << 5;
        self.cr1.0 |= bits;
        self.cr1.1 |= 0x60;
        self
    }

    pub fn set_clock_division(mut self, value: ClockDivision) -> Self {
        let bits = (value as u32) << 8;
        self.cr1.0 |= bits;
        self.cr1.1 |= 0x300;
        self
    }

    pub fn set_master_mode(mut self, value: MasterMode) -> Self {
        let bits = (value as u32) << 4;
        self.cr2.0 |= bits;
        self.cr2.1 |= 0x70;
        self
    }

    fn modify(address: u32, tuple: (u32, u32, u32)) {
        let addr = address as *mut u32;
        unsafe { modify_volatile(
            addr,
            |x| ((x & !tuple.1) | tuple.0) ^ tuple.2
        )}
    }

    pub fn execute(self) {
        match self.cr1 {
            (0, 0, 0) => (),
            _ => Self::modify(self.tim.base_address + CR1_OFFSET, self.cr1),
        }

        match self.cr2 {
            (0, 0, 0) => (),
            _ => Self::modify(self.tim.base_address + CR2_OFFSET, self.cr2),
        }

        match self.dier {
            (0, 0, 0) => (),
            _ => Self::modify(self.tim.base_address + DIER_OFFSET, self.dier),
        }
    }
}

pub struct StatusWriter<'a> {
    tim: &'a Tim,
    /// Set, clear and toggle
    value: (u32, u32, u32)
}

impl StatusWriter<'_> {
    pub fn set(mut self, flag: Status) -> Self {
        self.value.0 |= 1 << (flag as u8);
        self
    }

    pub fn clear(mut self, flag: Status) -> Self {
        self.value.1 |= 1 << (flag as u8);
        self
    }

    pub fn toggle(mut self, flag: Status) -> Self {
        self.value.2 |= 1 << (flag as u8);
        self
    }

    pub fn execute(self) {
        let address = (self.tim.base_address + SR_OFFSET) as *mut u32;
        unsafe { modify_volatile(
            address,
            |x| ((x & !self.value.1) | self.value.0) ^ self.value.2
        )};
    }
}

pub struct Tim { base_address: u32 }

impl Tim {
    pub fn control_write(&self) -> ControlWriter {
        ControlWriter { tim: self, cr1: (0, 0, 0), cr2: (0, 0, 0), dier: (0, 0, 0) }
    }

    pub fn set_counter(&self, value: u16) {
        let address = (self.base_address + CNT_OFFSET) as *mut u16;
        unsafe { write_volatile(address, value) }
    }

    pub fn set_prescaler(&self, value: u16) {
        let address = (self.base_address + PSC_OFFSET) as *mut u16;
        unsafe { write_volatile(address, value) }
    }

    pub fn set_reload(&self, value: u16) {
        let address = (self.base_address + ARR_OFFSET) as *mut u16;
        unsafe { write_volatile(address, value) }
    }

    pub fn status_write(&self) -> StatusWriter {
        StatusWriter { tim: self, value: (0, 0, 0) }
    }
}

pub const TIM2: Tim = Tim { base_address: 0x40000000 };
pub const TIM3: Tim = Tim { base_address: 0x40000400 };
pub const TIM4: Tim = Tim { base_address: 0x40000800 };
