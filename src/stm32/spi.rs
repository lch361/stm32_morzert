pub struct Spi { base_address: u32 }

pub const SPI1: Spi = Spi { base_address: 0x40013000 };
pub const SPI2: Spi = Spi { base_address: 0x40003800 };
