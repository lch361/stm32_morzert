use crate::extra::{self, ptr::modify_volatile};

const IMR_OFFSET: u32 = 0x00;
const EMR_OFFSET: u32 = 0x04;
const RTSR_OFFSET: u32 = 0x08;
const FTSR_OFFSET: u32 = 0x0C;
const SWIER_OFFSET: u32 = 0x10;
const PR_OFFSET: u32 = 0x14;

pub struct ExtiLines<'a> { exti: &'a Exti, set: u32, clear: u32 }

impl ExtiLines<'_> {
    pub fn set(mut self, line: u8) -> Self {
        self.set |= 1 << line;
        self
    }

    pub fn clear(mut self, line: u8) -> Self {
        self.clear |= 1 << line;
        self
    }

    pub fn interrupt(self) {
        let address = (self.exti.base_address + IMR_OFFSET) as *mut u32;
        unsafe { modify_volatile(address, |x| x | self.set & !self.clear) }
    }

    pub fn event(self) {
        let address = (self.exti.base_address + EMR_OFFSET) as *mut u32;
        unsafe { modify_volatile(address, |x| x | self.set & !self.clear) }
    }

    pub fn rising_trigger(self) {
        let address = (self.exti.base_address + RTSR_OFFSET) as *mut u32;
        unsafe { modify_volatile(address, |x| x | self.set & !self.clear) }
    }

    pub fn falling_trigger(self) {
        let address = (self.exti.base_address + FTSR_OFFSET) as *mut u32;
        unsafe { modify_volatile(address, |x| x | self.set & !self.clear) }
    }

    pub fn pending(self) {
        if self.set != 0 {
            let address = (self.exti.base_address + SWIER_OFFSET) as *mut u32;
            unsafe { modify_volatile(address, |x| x | self.set) }
        }

        if self.clear != 0 {
            let address = (self.exti.base_address + PR_OFFSET) as *mut u32;
            unsafe { modify_volatile(address, |x| x | self.clear) }
        }
    }
}

pub struct Exti { base_address: u32 }

impl Exti {
    pub fn lines(&self) -> ExtiLines {
        ExtiLines { exti: self, set: 0, clear: 0 }
    }
}

pub const EXTI: Exti = Exti { base_address: 0x40010400 };
