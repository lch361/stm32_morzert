pub struct I2c { base_address: u32 }

pub const I2C1: I2c = I2c { base_address: 0x40005400 };
pub const I2C2: I2c = I2c { base_address: 0x40005800 };
