use crate::cortex_m3;
use core::convert::TryFrom;

pub enum Vector {
    Watchdog,
    PVD,
    Tamper,
    RTC,
    Flash,
    RCC,
    EXTI0,
    EXTI1,
    EXTI2,
    EXTI3,
    EXTI4,
    DMA1Channel1,
    DMA1Channel2,
    DMA1Channel3,
    DMA1Channel4,
    DMA1Channel5,
    DMA1Channel6,
    DMA1Channel7,
    ADC1_2,
    UsbHpCanTx,
    UsbHpCanRx0,
    CanRX1,
    CanSCE,
    EXTI9_5,
    TIM1Brk,
    TIM1Up,
    TIM1TrgCom,
    TIM1Cc,
    TIM2,
    TIM3,
    TIM4,
    I2C1Ev,
    I2C1Er,
    I2C2Ev,
    I2C2Er,
    SPI1,
    SPI2,
    USART1,
    USART2,
    USART3,
    EXTI15_10,
    RTCAlarm,
    USBWakeup,
    TIM8Brk,
    TIM8Up,
    TIM8TrgCom,
    TIM8Cc,
    ADC3,
    FSMC,
    SDIO,
    TIM5,
    SPI3,
    UART4,
    UART5,
    TIM6,
    TIM7,
    DMA2Channel1,
    DMA2Channel2,
    DMA2Channel3,
    DMA2Channel4_5,
    Length,
}

impl const cortex_m3::boot::Into<cortex_m3::boot::Vector> for Vector {
    fn into(self) -> cortex_m3::boot::Vector {
        unsafe {
            core::mem::transmute((self as u8) + (cortex_m3::boot::Vector::IRQ0 as u8))
        }
    }
}

impl TryFrom<cortex_m3::boot::Vector> for Vector {
    type Error = ();
    fn try_from(src: cortex_m3::boot::Vector) -> Result<Self, Self::Error> {
        let (src_u8, irq0_u8) = (src as u8, cortex_m3::boot::Vector::IRQ0 as u8);
        if (src_u8 < irq0_u8) {
            Err(())
        } else {
            unsafe { Ok(core::mem::transmute(src_u8 - irq0_u8)) }
        }
    }
}
