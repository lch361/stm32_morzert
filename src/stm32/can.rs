pub struct Can { base_address: u32 }

pub const CAN: Can = Can { base_address: 0x40006400 };
