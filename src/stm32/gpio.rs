use crate::extra;

const CR_OFFSET: u32 = 0x00;
const IDR_OFFSET: u32 = 0x08;
const ODR_OFFSET: u32 = 0x0C;

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Output {
    PushPull,
    OpenDrain,
    AFPushPull,
    AFOpenDrain,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Input {
    Analog,
    Floating,
    Pull,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum MaxSpeed {
    Mhz10 = 0x1,
    Mhz2,
    Mhz50,
}

pub struct PinsReader { value: u32 }

impl PinsReader {
    pub fn is_set(&self, pin: u8) -> bool {
        self.value & (1 << pin) != 0
    }
}

pub struct PinsWriter<'a> {
    gpio: &'a Gpio,
    value: u32,
    mask: u32,
    xor: u32,
}

impl PinsWriter<'_> {
    pub fn set(mut self, pin: u8) -> Self {
        self.value |= 1 << pin;
        self
    }

    pub fn clear(mut self, pin: u8) -> Self {
        self.mask |= 1 << pin;
        self
    }

    pub fn toggle(mut self, pin: u8) -> Self {
        self.xor |= 1 << pin;
        self
    }

    pub fn execute(self) {
        let address = (self.gpio.base_address + ODR_OFFSET) as *mut u32;
        unsafe {
            extra::ptr::modify_volatile(
                address,
                |x| ((x & !self.mask) | self.value) ^ self.xor
            );
        }
    }
}

pub struct ConfigWriter<'a> {
    gpio: &'a Gpio,
    value: u64,
    mask: u64,
}

impl ConfigWriter<'_> {
    pub fn output(mut self, pin: u8, output: Output, speed: MaxSpeed) -> Self {
        let value = (output as u64) << 2 | (speed as u64);
        let offset = pin * 4;
        self.value |= value << offset;
        self.mask &= !(0xF << offset);
        self
    }

    pub fn input(mut self, pin: u8, input: Input) -> Self {
        let value = (input as u64) << 2;
        let offset = pin * 4;
        self.value |= value << offset;
        self.mask &= !(0xF << offset);
        self
    }

    pub fn execute(self) {
        let addr = (self.gpio.base_address + CR_OFFSET) as *mut u64;
        unsafe {
            extra::ptr::modify_volatile(addr, |val| (val & self.mask) | self.value);
        }
    }
}

pub struct Gpio {
    base_address: u32,
}

impl Gpio {
    pub fn config_write(&self) -> ConfigWriter {
        ConfigWriter { gpio: self, value: 0, mask: u64::MAX }
    }

    pub fn pins_read(&self) -> PinsReader {
        let address = (self.base_address + IDR_OFFSET) as *const u32;
        let value = unsafe { core::ptr::read_volatile(address) };
        PinsReader { value }
    }

    pub fn pins_write(&self) -> PinsWriter {
        PinsWriter { gpio: self, value: 0, mask: 0, xor: 0 }
    }
}

pub const GPIOA: Gpio = Gpio { base_address: 0x40010800 };
pub const GPIOB: Gpio = Gpio { base_address: 0x40010C00 };
pub const GPIOC: Gpio = Gpio { base_address: 0x40011000 };
pub const GPIOD: Gpio = Gpio { base_address: 0x40011400 };
pub const GPIOE: Gpio = Gpio { base_address: 0x40011800 };
