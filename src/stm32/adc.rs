pub struct Adc { base_address: u32 }

pub const ADC1: Adc = Adc { base_address: 0x40012400 };
pub const ADC2: Adc = Adc { base_address: 0x40012800 };
