#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Apb1 {
    Tim2 = 0,
    Tim3 = 1,
    Tim4 = 2,
    Tim5 = 3,
    Tim6 = 4,
    Tim7 = 5,
    Tim12 = 6,
    Tim13 = 7,
    Tim14 = 8,
    Wwdg = 11,
    Spi2 = 14,
    Spi3 = 15,
    Usart2 = 17,
    Usart3 = 18,
    Uart4 = 19,
    Uart5 = 20,
    I2c1 = 21,
    I2c2 = 22,
    Usb = 23,
    Can = 25,
    Bkp = 27,
    Pwr = 28,
    Dac = 29,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Apb2 {
    Afio = 0,
    Iopa = 2,
    Iopb = 3,
    Iopc = 4,
    Iopd = 5,
    Iope = 6,
    Iopf = 7,
    Iopg = 8,
    Adc1 = 9,
    Adc2 = 10,
    Tim1 = 11,
    Spi1 = 12,
    Tim8 = 13,
    Usart1 = 14,
    Adc3 = 15,
    Tim9 = 19,
    Tim10 = 20,
    Tim11 = 21,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Ahb {
    Dma1 = 0,
    Dma2 = 1,
    Sram = 2,
    Flitf = 4,
    Crc = 6,
    Fsmc = 8,
    Sdio = 10,
}

pub enum DeviceType {
    Apb2, Apb1, Ahb
}

pub trait Device {
    const TYPE: DeviceType;
    fn to_number(self) -> u32 where Self: Sized;
}

impl Device for Apb1 {
    const TYPE: DeviceType = DeviceType::Apb1;
    fn to_number(self) -> u32 { 1 << (self as u8) }
}

impl Device for Apb2 {
    const TYPE: DeviceType = DeviceType::Apb2;
    fn to_number(self) -> u32 { 1 << (self as u8) }
}

impl Device for Ahb {
    const TYPE: DeviceType = DeviceType::Ahb;
    fn to_number(self) -> u32 { 1 << (self as u8) }
}

pub trait DeviceReset where Self: Device { }

impl DeviceReset for Apb1 { }

impl DeviceReset for Apb2 { }
