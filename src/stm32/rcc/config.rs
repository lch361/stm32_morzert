use crate::extra;

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum McoSrc {
    No = 0x0,
    Sysclk = 0x4,
    Hsi = 0x5,
    PllDiv2 = 0x7,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum UsbScaler {
    PllDiv1_5, Pll
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum PllScaler {
    Mul2, Mul3, Mul4, Mul5, Mul6, Mul7, Mul8,
    Mul9, Mul10, Mul11, Mul12, Mul13, Mul14, Mul15, Mul16
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum PllSrc {
    HsiDiv2, Hse
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum PllHseScaler {
    Div1, Div2
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Apb2Scaler {
    HclkDiv1,
    HclkDiv2 = 0x4,
    HclkDiv4, HclkDiv8, HclkDiv16,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Apb1Scaler {
    HclkDiv1,
    HclkDiv2 = 0x4,
    HclkDiv4, HclkDiv8, HclkDiv16,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum AhbScaler {
    SysclkDiv1,
    SysclkDiv2 = 0x8,
    SysclkDiv4, SysclkDiv8, SysclkDiv16, SysclkDiv64,
    SysclkDiv128, SysclkDiv256, SysclkDiv512,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum AdcScaler {
    Apb2Div2, Apb2Div4, Apb2Div6, Apb2Div8,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum SysclkSrc {
    Hsi, Hse, Pll, No
}

pub trait ConfigElement {
    const OFFSET: u8;
    const MASK_WIDTH: u8;

    fn to_value(self) -> u32 where Self: Sized;
    fn to_mask_value(self) -> (u32, u32) where Self: Sized {
        let mask = (1 << Self::MASK_WIDTH) - 1;
        (mask << Self::OFFSET, (self.to_value()) << Self::OFFSET)
    }
}

macro_rules! implement_config_element {
    ($enum:ty, $offset:expr, $width: expr) => {
        impl ConfigElement for $enum {
            const OFFSET: u8 = $offset;
            const MASK_WIDTH: u8 = $width;
            fn to_value(self) -> u32 { self as u32 }
        }
    };
}

implement_config_element!(McoSrc, 24, 3);
implement_config_element!(UsbScaler, 22, 1);
implement_config_element!(PllScaler, 18, 4);
implement_config_element!(PllHseScaler, 17, 1);
implement_config_element!(PllSrc, 16, 1);
implement_config_element!(AdcScaler, 14, 2);
implement_config_element!(Apb2Scaler, 11, 3);
implement_config_element!(Apb1Scaler, 8, 3);
implement_config_element!(AhbScaler, 4, 4);
implement_config_element!(SysclkSrc, 0, 2);
