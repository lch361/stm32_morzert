#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Flags {
    HsiOn,
    HsiReady,
    HseOn = 16,
    HseReady,
    HseBypass,
    CssOn,
    PllOn = 24,
    PllReady
}
