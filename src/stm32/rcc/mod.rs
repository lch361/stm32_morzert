pub mod control;
pub mod config;
pub mod device;

use crate::extra;

const CR_OFFSET: u32 = 0x00;
const CFGR_OFFSET: u32 = 0x04;
const AHBENR_OFFSET: u32 = 0x14;
const APB2ENR_OFFSET: u32 = 0x18;
const APB1ENR_OFFSET: u32 = 0x1C;
const APB2RSTR_OFFSET: u32 = 0x0C;
const APB1RSTR_OFFSET: u32 = 0x10;

pub struct ControlReader { value: u32 }

impl ControlReader {
    pub fn get_hsi_calibration(&self) -> u8 {
        ((self.value & 0xFF00) >> 8) as u8
    }

    pub fn is_set(&self, flag: control::Flags) -> bool {
        self.value & (1 << flag as u8) != 0
    }
}

pub struct ControlWriter<'a> {
    rcc: &'a Rcc,
    value: u32,
    mask: u32,
}

impl ControlWriter<'_> {
    pub fn set(mut self, flag: control::Flags) -> Self {
        self.value |= 1 << flag as u8;
        self
    }

    pub fn clear(mut self, flag: control::Flags) -> Self {
        self.mask |= 1 << flag as u8;
        self
    }

    pub fn set_hsi_trimming(mut self, value: u8) -> Self {
        self.mask |= 0xF8;
        self.value |= (value as u32) & 0x1F;
        self
    }

    pub fn execute(self) {
        let address = (self.rcc.base_address + CR_OFFSET) as *mut u32;
        unsafe {
            extra::ptr::modify_volatile(address, |x| (x & !self.mask) | self.value);
        }
    }
}

pub struct Devices<'a> {
    rcc: &'a Rcc,
    /// Set, clear, toggle and reset
    apb2: (u32, u32, u32, u32),
    /// Set, clear, toggle and reset
    apb1: (u32, u32, u32, u32),
    /// Set, clear and toggle
    ahb: (u32, u32, u32),
}

// Alternate impl for Devices
impl Devices<'_> {
    pub fn enable<D: device::Device>(mut self, value: D) -> Self {
        let v = value.to_number();
        match D::TYPE {
            device::DeviceType::Ahb => self.ahb.0 |= v,
            device::DeviceType::Apb2 => self.apb2.0 |= v,
            device::DeviceType::Apb1 => self.apb1.0 |= v,
        }
        self
    }

    pub fn disable<D: device::Device>(mut self, value: D) -> Self {
        let v = value.to_number();
        match D::TYPE {
            device::DeviceType::Ahb => self.ahb.1 |= v,
            device::DeviceType::Apb2 => self.apb2.1 |= v,
            device::DeviceType::Apb1 => self.apb1.1 |= v,
        }
        self
    }

    pub fn toggle<D: device::Device>(mut self, value: D) -> Self {
        let v = value.to_number();
        match D::TYPE {
            device::DeviceType::Ahb => self.ahb.2 |= v,
            device::DeviceType::Apb2 => self.apb2.2 |= v,
            device::DeviceType::Apb1 => self.apb1.2 |= v,
        }
        self
    }

    pub fn reset<D: device::DeviceReset>(mut self, value: D) -> Self {
        let v = value.to_number();
        match D::TYPE {
            device::DeviceType::Apb2 => self.apb2.3 |= v,
            device::DeviceType::Apb1 => self.apb1.3 |= v,
            _ => ()
        }
        self
    }

    pub fn execute(self) {
        macro_rules! modify {
            ($offset:expr, $closure:expr) => {
                let address = (self.rcc.base_address + $offset) as *mut u32;
                unsafe {
                    extra::ptr::modify_volatile(address, $closure);
                }
            };
        }

        if let (0, 0, 0, _) = self.apb2 { } else {
            modify!(APB2ENR_OFFSET, |x| ((x & !self.apb2.1) | self.apb2.0) ^ self.apb2.2);
        }

        if let (0, 0, 0, _) = self.apb1 { } else {
            modify!(APB1ENR_OFFSET, |x| ((x & !self.apb1.1) | self.apb1.0) ^ self.apb1.2);
        }

        if let (0, 0, 0) = self.ahb { } else {
            modify!(APB1ENR_OFFSET, |x| ((x & !self.ahb.1) | self.ahb.0) ^ self.ahb.2);
        }

        if self.apb1.3 != 0 {
            modify!(APB1RSTR_OFFSET, |x| x | self.apb1.3);
        }

        if self.apb2.3 != 0 {
            modify!(APB2RSTR_OFFSET, |x| x | self.apb2.3);
        }
    }
}

pub struct ConfigReader { value: u32 }

impl ConfigReader {
    pub fn get_system_clock_switch(&self) -> config::SysclkSrc {
        let sws = (self.value & 0xC) >> 2;
        match sws {
            0x0 => config::SysclkSrc::Hsi,
            0x1 => config::SysclkSrc::Hse,
            0x2 => config::SysclkSrc::Pll,
            _   => config::SysclkSrc::No
        }
    }
}

pub struct ConfigWriter<'a> {
    rcc: &'a Rcc,
    value: u32,
    mask: u32,
}

impl ConfigWriter<'_> {
    pub fn set<C: config::ConfigElement>(mut self, element: C) -> Self {
        let (mask, value) = element.to_mask_value();
        self.value |= value;
        self.mask |= mask;
        self
    }

    pub fn execute(self) {
        let address = (self.rcc.base_address + CFGR_OFFSET) as *mut u32;
        unsafe {
            extra::ptr::modify_volatile(address, |x| (x & !self.mask) | self.value)
        }
    }
}

pub struct Rcc {
    base_address: u32,
}

impl Rcc {
    pub fn config_write(&self) -> ConfigWriter {
        ConfigWriter { rcc: self, value: 0, mask: 0 }
    }

    pub fn config_read(&self) -> ConfigReader {
        let address = (self.base_address + CFGR_OFFSET) as *const u32;
        let value = unsafe { core::ptr::read_volatile(address) };
        ConfigReader { value }
    }

    pub fn devices(&self) -> Devices {
        Devices { rcc: self, apb2: (0, 0, 0, 0), apb1: (0, 0, 0, 0), ahb: (0, 0, 0) }
    }

    pub fn control_write(&self) -> ControlWriter {
        ControlWriter { rcc: self, value: 0, mask: 0 }
    }

    pub fn control_read(&self) -> ControlReader {
        let address = (self.base_address + CR_OFFSET) as *const u32;
        let value = unsafe { core::ptr::read_volatile(address) };
        ControlReader { value }
    }
}

pub const RCC: Rcc = Rcc {
    base_address: 0x40021000,
};
