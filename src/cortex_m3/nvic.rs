use crate::{cortex_m3::boot::Vector, extra};
use crate::cortex_m3::boot::Into as CortexInto;

const ISER_OFFSET: u32 = 0x00;
const ICER_OFFSET: u32 = 0x80;
const ISPR_OFFSET: u32 = 0x100;
const ICPR_OFFSET: u32 = 0x180;
const IABR_OFFSET: u32 = 0x200;
const IPR_OFFSET: u32 = 0x300;
const STIR_OFFSET: u32 = 0xE00;

pub struct Nvic {
    base_address: u32,
}

impl Nvic {
    fn vector_to_irqnum(vec: Vector) -> Option<u8> {
        let result = vec as u8;
        if result < Vector::IRQ0 as u8 {
            None
        } else {
            Some(result - Vector::IRQ0 as u8)
        }
    }

    fn write_bit(&self, irq: Vector, offset: u32) {
        let irqnum = match Self::vector_to_irqnum(irq) {
            None => return,
            Some(v) => v,
        };

        let (off, bit) = (((irqnum / 32) * 4) as u32, 1_u32 << (irqnum % 32));

        let address = (self.base_address + offset + off) as *mut u32;
        unsafe { core::ptr::write_volatile(address, bit) }
    }

    fn read_bit(&self, irq: Vector, offset: u32) -> bool {
        let irqnum = match Self::vector_to_irqnum(irq) {
            None => return false,
            Some(v) => v,
        };

        let (off, bit) = (((irqnum / 32) * 4) as u32, 1_u32 << (irqnum % 32));

        let address = (self.base_address + offset + off) as *const u32;
        bit & unsafe { core::ptr::read_volatile(address) } != 0
    }

    pub fn generate_sgi<V: CortexInto<Vector>>(&self, irq: V) {
        let irqnum = match Self::vector_to_irqnum(irq.into()) {
            None => return,
            Some(v) => v,
        };

        let address = (self.base_address + STIR_OFFSET) as *mut u32;
        unsafe {
            core::ptr::write_volatile(address, irqnum as u32)
        }
    }

    pub fn enable<V: CortexInto<Vector>>(&self, irq: V) {
        self.write_bit(irq.into(), ISER_OFFSET)
    }

    pub fn disable<V: CortexInto<Vector>>(&self, irq: V) {
        self.write_bit(irq.into(), ICER_OFFSET)
    }

    pub fn set_pending<V: CortexInto<Vector>>(&self, irq: V) {
        self.write_bit(irq.into(), ISPR_OFFSET)
    }

    pub fn clear_pending<V: CortexInto<Vector>>(&self, irq: V) {
        self.write_bit(irq.into(), ICPR_OFFSET)
    }

    pub fn active<V: CortexInto<Vector>>(&self, irq: V) -> bool {
        self.read_bit(irq.into(), IABR_OFFSET)
    }

    pub fn pending<V: CortexInto<Vector>>(&self, irq: V) -> bool {
        self.read_bit(irq.into(), ISPR_OFFSET)
    }

    pub fn enabled<V: CortexInto<Vector>>(&self, irq: V) -> bool {
        self.read_bit(irq.into(), ISER_OFFSET)
    }

    pub fn get_priority<V: CortexInto<Vector>>(&self, irq: V) -> Option<u8> {
        let irqnum = match Self::vector_to_irqnum(irq.into()) {
            None => return None,
            Some(v) => v,
        } as u32;

        let address = (self.base_address + IPR_OFFSET + irqnum) as *const u8;
        Some(unsafe { core::ptr::read_volatile(address) } >> 4)
    }

    pub fn set_priority<V: CortexInto<Vector>>(&self, irq: V, priority: u8) {
        let irqnum = match Self::vector_to_irqnum(irq.into()) {
            None => return,
            Some(v) => v,
        } as u32;

        let address = (self.base_address + IPR_OFFSET + irqnum) as *mut u8;
        unsafe { core::ptr::write_volatile(address, priority << 4) }
    }
}

pub const NVIC: Nvic = Nvic { base_address: 0xE000E100 };
