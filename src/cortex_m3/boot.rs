#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Vector {
    Reset = 0,
    NMI,
    HardFault,
    MMFault,
    BusFault,
    UsageFault,
    SVCall = 10,
    DebugMonitor,
    PendSV = 13,
    SysTick,
    IRQ0,
    IRQ1,
    IRQ2,
    IRQ3,
    IRQ4,
    IRQ5,
    IRQ6,
    IRQ7,
    IRQ8,
    IRQ9,
    IRQ10,
    IRQ11,
    IRQ12,
    IRQ13,
    IRQ14,
    IRQ15,
    IRQ16,
    IRQ17,
    IRQ18,
    IRQ19,
    IRQ20,
    IRQ21,
    IRQ22,
    IRQ23,
    IRQ24,
    IRQ25,
    IRQ26,
    IRQ27,
    IRQ28,
    IRQ29,
    IRQ30,
    IRQ31,
    IRQ32,
    IRQ33,
    IRQ34,
    IRQ35,
    IRQ36,
    IRQ37,
    IRQ38,
    IRQ39,
    IRQ40,
    IRQ41,
    IRQ42,
    IRQ43,
    IRQ44,
    IRQ45,
    IRQ46,
    IRQ47,
    IRQ48,
    IRQ49,
    IRQ50,
    IRQ51,
    IRQ52,
    IRQ53,
    IRQ54,
    IRQ55,
    IRQ56,
    IRQ57,
    IRQ58,
    IRQ59,
    Length,
}

#[const_trait]
pub trait Into<Vector> {
    fn into(self) -> Vector;
}

impl const Into<Vector> for Vector {
    fn into(self) -> Vector {
        self
    }
}

pub type VectorFunction = unsafe extern "C" fn();

/* Usually set by 0 in linker script */
extern "C" {
    fn null_vector();
}

#[repr(C)]
pub struct Sector {
    stack_top: u32,
    vector: [VectorFunction; Vector::Length as usize],
}

impl Sector {
    pub const fn new(stack_top: u32) -> Sector {
        let mut result = Sector {
            stack_top,
            vector: [null_vector; Vector::Length as usize],
        };

        result
    }

    pub const fn add<V>(mut self, vector: V, function: VectorFunction) -> Self
    where V: ~const Into<Vector> {
        self.vector[vector.into() as usize] = function;
        self
    }
}
