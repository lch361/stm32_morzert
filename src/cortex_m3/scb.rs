use crate::cortex_m3::boot;
use crate::extra;

const AIRCR_OFFSET: u32 = 0x0C;
const ICSR_OFFSET: u32 = 0x04;
const SCR_OFFSET: u32 = 0x10;
const SHPR_OFFSET: u32 = 0x18;
const SHCSR_OFFSET: u32 = 0x24;

pub struct Scb {
    base_address: u32
}

impl Scb {
    pub fn reset_system(&self) -> ! {
        let addr = (self.base_address + AIRCR_OFFSET) as *mut u32;
        unsafe {
            extra::ptr::modify_volatile(addr, |x| x ^ 0xFFFF0004);
        }
        loop {}
    }

    fn get_priority_index(vector: boot::Vector) -> Option<u8> {
        match vector {
            boot::Vector::BusFault|boot::Vector::MMFault|boot::Vector::UsageFault|
                boot::Vector::PendSV|boot::Vector::SVCall|
                boot::Vector::SysTick => Some(
                    (vector as u8) - (boot::Vector::MMFault as u8)
                ),
            _ => None,
        }
    }

    pub fn get_active(&self) -> u16 {
        let address = (self.base_address + ICSR_OFFSET) as *const u32;
        let value = unsafe {
            core::ptr::read_volatile(address)
        };
        (value & 0x1FF) as u16
    }

    pub fn get_pending(&self) -> u16 {
        let address = (self.base_address + ICSR_OFFSET) as *const u32;
        let value = unsafe {
            core::ptr::read_volatile(address)
        };
        (value & 0x003FF00 >> 12) as u16
    }

    pub fn get_priority(&self, vector: boot::Vector) -> Option<u8> {
        let priority_index = match Self::get_priority_index(vector) {
            None => return None,
            Some(v) => v
        } as u32;

        let address = (self.base_address + SHPR_OFFSET + priority_index) as *const u8;

        let value = unsafe {
            core::ptr::read_volatile(address)
        };
        Some(value >> 4)
    }

    pub fn set_priority(&self, vector: boot::Vector, priority: u8) {
        let priority_index = match Self::get_priority_index(vector) {
            None => return,
            Some(v) => v
        } as u32;

        let address = (self.base_address + SHPR_OFFSET + priority_index) as *mut u8;

        unsafe {
            core::ptr::write_volatile(address, priority << 4);
        }
    }

    pub fn set_sleep_on_exit(&self) {
        let address = (self.base_address + SCR_OFFSET) as *mut u8;
        unsafe {
            extra::ptr::modify_volatile(address, |val| val | 1 << 1);
        }
    }
}

pub const SCB: Scb = Scb {
    base_address: 0xE000ED00
};
