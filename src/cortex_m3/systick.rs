use crate::extra;

const CTRL_OFFSET: u32 = 0x00;
const LOAD_OFFSET: u32 = 0x04;
const VAL_OFFSET: u32 = 0x08;

pub struct SysTick {
    base_address: u32,
}

#[repr(u8)]
#[derive(Clone, Copy)]
pub enum Config {
    Enabled,
    AssertIRQ,
    UseAHB,
}

pub struct ConfigWriter<'a> {
    systick: &'a SysTick,
    add: u32,
    delete: u32,
    toggle: u32,
}

impl ConfigWriter<'_> {
    pub fn set(mut self, value: Config) -> Self {
        self.add |= 1 << (value as u8);
        self
    }

    pub fn clear(mut self, value: Config) -> Self {
        self.delete |= 1 << (value as u8);
        self
    }

    pub fn toggle(mut self, value: Config) -> Self {
        self.toggle |= 1 << (value as u8);
        self
    }

    pub fn execute(self) {
        let address = (self.systick.base_address + CTRL_OFFSET) as *mut u32;
        unsafe {
            extra::ptr::modify_volatile(
                address,
                |x| ((x & !self.delete) | self.add) ^ self.toggle
            )
        }
    }
}

impl SysTick {
    pub fn value(&self) -> u32 {
        let address = (self.base_address + VAL_OFFSET) as *const u32;
        unsafe { core::ptr::read_volatile(address) }
    }

    pub fn value_clear(&self) {
        let address = (self.base_address + VAL_OFFSET) as *mut u32;
        unsafe {
            core::ptr::write_volatile(address, 0)
        }
    }

    pub fn reload_set(&self, value: u32, multishot: bool) {
        let address = (self.base_address + LOAD_OFFSET) as *mut u32;
        unsafe {
            core::ptr::write_volatile(address, value - (multishot as u32))
        }
    }

    pub fn config_write(&self) -> ConfigWriter {
        ConfigWriter { systick: self, add: 0, delete: 0, toggle: 0 }
    }

    pub fn countflag(&self) -> bool {
        let address = (self.base_address + CTRL_OFFSET) as *const u32;
        let val = unsafe { core::ptr::read_volatile(address) };
        val & (1 << 16) != 0
    }
}

pub const STK: SysTick = SysTick { base_address: 0xE000E010 };
