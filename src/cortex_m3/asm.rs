pub fn wfi() {
    unsafe {
        core::arch::asm!("wfi");
    }
}

pub fn nop() {
    unsafe {
        core::arch::asm!("nop");
    }
}
