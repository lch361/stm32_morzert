use core::{convert::TryInto, ops::Index};

const MAX_MORZE_LENGTH: u8 = 7;
const ASCII_MAX: usize = 128;

#[derive(Clone, Copy)]
pub struct MorzeCode {
    length: u8,
    data: u8,
}

impl MorzeCode {
    pub const fn new(bit: bool) -> Self {
        Self { length: 1, data: bit as u8 }
    }

    /// Gives an error if length would exceed 8
    pub fn push(mut self, bit: bool) -> Result<Self, ()> {
        if self.length == MAX_MORZE_LENGTH {
            Err(())
        } else {
            self.length += 1;
            self.data = (self.data << 1) | (bit as u8);
            Ok(self)
        }
    }
}

const fn filled_bits(bit: u8) -> usize {
    ((1_u32 << bit) - 1) as usize
}

const TOTAL_CHAR_ARRAY_LENGTH: usize = filled_bits(MAX_MORZE_LENGTH + 1) - 1;

struct MorzeTable {
    char_to_morze: [Option<MorzeCode>; ASCII_MAX],
    morze_to_char: [Option<u8>; TOTAL_CHAR_ARRAY_LENGTH],
}

impl MorzeTable {
    const fn calculate_offset(morze_code_length: u8) -> usize {
        filled_bits(morze_code_length) - 1
    }

    pub const fn add_match(mut self, char: u8, morze: MorzeCode) -> Self {
        self.char_to_morze[char as usize] = Some(morze);
        let offset = Self::calculate_offset(morze.length);
        self.morze_to_char[offset + morze.data as usize] = Some(char);
        self
    }

    pub const fn new() -> Self {
        let mut result = Self {
            char_to_morze: [None; ASCII_MAX],
            morze_to_char: [None; TOTAL_CHAR_ARRAY_LENGTH],
        };

        result
    }
}

impl Index<MorzeCode> for MorzeTable {
    type Output = Option<u8>;
    fn index(&self, index: MorzeCode) -> &Self::Output {
        let offset = Self::calculate_offset(index.length);
        let result = unsafe {
            MORZE_TABLE.morze_to_char.get_unchecked(offset + index.data as usize)
        };
        result
    }
}

impl Index<u8> for MorzeTable {
    type Output = Option<MorzeCode>;
    fn index(&self, index: u8) -> &Self::Output {
        let result = unsafe {
            MORZE_TABLE.char_to_morze.get_unchecked(index as usize)
        };
        result
    }
}

impl TryInto<u8> for MorzeCode {
    type Error = ();
    fn try_into(self) -> Result<u8, Self::Error> {
        match MORZE_TABLE[self] {
            None    => Err(()),
            Some(v) => Ok(v),
        }
    }
}

impl Into<MorzeCode> for u8 {
    fn into(self) -> MorzeCode {
        match MORZE_TABLE[self] {
            None    => MorzeCode { length: 8, data: 0b11111111 },
            Some(v) => v,
        }
    }
}

pub struct MorzeCodeIterator { length: u8, data: u8 }

impl Iterator for MorzeCodeIterator {
    type Item = bool;
    fn next(&mut self) -> Option<Self::Item> {
        if self.length == 0 {
            None
        } else {
            let result = self.data & (1 << (self.length - 1)) != 0;
            self.length -= 1;
            Some(result)
        }
    }
}

impl IntoIterator for MorzeCode {
    type Item = bool;
    type IntoIter = MorzeCodeIterator;
    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter { length: self.length, data: self.data }
    }
}

const MORZE_TABLE: MorzeTable = MorzeTable::new()
    .add_match(b'A' , MorzeCode { length: 2, data: 0b01 })
    .add_match(b'a' , MorzeCode { length: 2, data: 0b01 })
    .add_match(b'B' , MorzeCode { length: 4, data: 0b1000 })
    .add_match(b'b' , MorzeCode { length: 4, data: 0b1000 })
    .add_match(b'C' , MorzeCode { length: 4, data: 0b1010 })
    .add_match(b'c' , MorzeCode { length: 4, data: 0b1010 })
    .add_match(b'D' , MorzeCode { length: 3, data: 0b100 })
    .add_match(b'd' , MorzeCode { length: 3, data: 0b100 })
    .add_match(b'E' , MorzeCode { length: 1, data: 0b0 })
    .add_match(b'e' , MorzeCode { length: 1, data: 0b0 })
    .add_match(b'F' , MorzeCode { length: 4, data: 0b0010 })
    .add_match(b'f' , MorzeCode { length: 4, data: 0b0010 })
    .add_match(b'G' , MorzeCode { length: 3, data: 0b110 })
    .add_match(b'g' , MorzeCode { length: 3, data: 0b110 })
    .add_match(b'H' , MorzeCode { length: 4, data: 0b0000 })
    .add_match(b'h' , MorzeCode { length: 4, data: 0b0000 })
    .add_match(b'I' , MorzeCode { length: 2, data: 0b00 })
    .add_match(b'i' , MorzeCode { length: 2, data: 0b00 })
    .add_match(b'J' , MorzeCode { length: 4, data: 0b0111 })
    .add_match(b'j' , MorzeCode { length: 4, data: 0b0111 })
    .add_match(b'K' , MorzeCode { length: 3, data: 0b101 })
    .add_match(b'k' , MorzeCode { length: 3, data: 0b101 })
    .add_match(b'L' , MorzeCode { length: 4, data: 0b0100 })
    .add_match(b'l' , MorzeCode { length: 4, data: 0b0100 })
    .add_match(b'M' , MorzeCode { length: 2, data: 0b11 })
    .add_match(b'm' , MorzeCode { length: 2, data: 0b11 })
    .add_match(b'N' , MorzeCode { length: 2, data: 0b10 })
    .add_match(b'n' , MorzeCode { length: 2, data: 0b10 })
    .add_match(b'O' , MorzeCode { length: 3, data: 0b111 })
    .add_match(b'o' , MorzeCode { length: 3, data: 0b111 })
    .add_match(b'P' , MorzeCode { length: 4, data: 0b0110 })
    .add_match(b'p' , MorzeCode { length: 4, data: 0b0110 })
    .add_match(b'R' , MorzeCode { length: 3, data: 0b010 })
    .add_match(b'r' , MorzeCode { length: 3, data: 0b010 })
    .add_match(b'S' , MorzeCode { length: 3, data: 0b000 })
    .add_match(b's' , MorzeCode { length: 3, data: 0b000 })
    .add_match(b'T' , MorzeCode { length: 1, data: 0b1 })
    .add_match(b't' , MorzeCode { length: 1, data: 0b1 })
    .add_match(b'U' , MorzeCode { length: 3, data: 0b001 })
    .add_match(b'u' , MorzeCode { length: 3, data: 0b001 })
    .add_match(b'V' , MorzeCode { length: 4, data: 0b0001 })
    .add_match(b'v' , MorzeCode { length: 4, data: 0b0001 })
    .add_match(b'W' , MorzeCode { length: 3, data: 0b011 })
    .add_match(b'w' , MorzeCode { length: 3, data: 0b011 })
    .add_match(b'Q' , MorzeCode { length: 4, data: 0b1101 })
    .add_match(b'q' , MorzeCode { length: 4, data: 0b1101 })
    .add_match(b'X' , MorzeCode { length: 4, data: 0b1001 })
    .add_match(b'x' , MorzeCode { length: 4, data: 0b1001 })
    .add_match(b'Y' , MorzeCode { length: 4, data: 0b1011 })
    .add_match(b'y' , MorzeCode { length: 4, data: 0b1011 })
    .add_match(b'Z' , MorzeCode { length: 4, data: 0b1100 })
    .add_match(b'z' , MorzeCode { length: 4, data: 0b1100 })
    .add_match(b'1' , MorzeCode { length: 5, data: 0b01111 })
    .add_match(b'2' , MorzeCode { length: 5, data: 0b00111 })
    .add_match(b'3' , MorzeCode { length: 5, data: 0b00011 })
    .add_match(b'4' , MorzeCode { length: 5, data: 0b00001 })
    .add_match(b'5' , MorzeCode { length: 5, data: 0b00000 })
    .add_match(b'6' , MorzeCode { length: 5, data: 0b10000 })
    .add_match(b'7' , MorzeCode { length: 5, data: 0b11000 })
    .add_match(b'8' , MorzeCode { length: 5, data: 0b11100 })
    .add_match(b'9' , MorzeCode { length: 5, data: 0b11110 })
    .add_match(b'0' , MorzeCode { length: 5, data: 0b11111 })
    .add_match(b'.' , MorzeCode { length: 6, data: 0b010101 })
    .add_match(b',' , MorzeCode { length: 6, data: 0b110011 })
    .add_match(b'?' , MorzeCode { length: 6, data: 0b001100 })
    .add_match(b'\\', MorzeCode { length: 6, data: 0b011110 })
    .add_match(b'!' , MorzeCode { length: 6, data: 0b101011 })
    .add_match(b')' , MorzeCode { length: 6, data: 0b101101 })
    .add_match(b':' , MorzeCode { length: 6, data: 0b111000 })
    .add_match(b';' , MorzeCode { length: 6, data: 0b101010 })
    .add_match(b'-' , MorzeCode { length: 6, data: 0b100001 })
    .add_match(b'_' , MorzeCode { length: 6, data: 0b001101 })
    .add_match(b'"' , MorzeCode { length: 6, data: 0b010010 })
    .add_match(b'@' , MorzeCode { length: 6, data: 0b011010 })
    .add_match(b'(' , MorzeCode { length: 5, data: 0b10110 })
    .add_match(b'/' , MorzeCode { length: 5, data: 0b10010 })
    .add_match(b'+' , MorzeCode { length: 6, data: 0b01010 })
    .add_match(b'&' , MorzeCode { length: 5, data: 0b01000 })
    .add_match(b'=' , MorzeCode { length: 5, data: 0b10001 })
    .add_match(b'$' , MorzeCode { length: 7, data: 0b0001001 })
    .add_match(0x4  , MorzeCode { length: 6, data: 0b000101 });
