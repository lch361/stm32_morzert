SRCDIR     := src
BUILDDIR   := build
SCRIPTSDIR := scripts
INCREMENTALDIR := $(BUILDDIR)/incremental

CTARGET := arm-none-eabi
LD := $(CTARGET)-ld
SZ := $(CTARGET)-size
OBJCOPY := $(CTARGET)-objcopy

RUSTTARGET := thumbv7m-none-eabi

RUSTC := rustc
OPT_LEVEL ?= 3
RUSTFLAGS += --target $(RUSTTARGET) \
	     -Cpanic=abort -Copt-level=$(OPT_LEVEL) \
	     -Ctarget-cpu=cortex-m3 -Cincremental=$(INCREMENTALDIR) \
	     -Clinker=$(LD) -Clink-args=-T$(LD_SCRIPT)

LD_SCRIPT := stm32f103c6t6.ld

rwildcard = $(foreach i,$(wildcard $1/*),$(filter $2,$i) $(call rwildcard,$(i:%/=%),$2))

SRCS := $(call rwildcard,$(SRCDIR),%.rs)

$(BUILDDIR)/main.bin: $(BUILDDIR)/main.elf
	$(OBJCOPY) $< $@ -O binary

$(BUILDDIR)/main.elf: $(SRCS) $(LD_SCRIPT) | $(BUILDDIR)/
	$(RUSTC) $(RUSTFLAGS) -o $@ $(SRCDIR)/main.rs
	$(SZ) $@

$(BUILDDIR)/:
	mkdir $@

rust-project.json: $(SCRIPTSDIR)/rust-project_gen
	$< > $@

clean:
	rm -rf $(BUILDDIR)

.PHONY: clean
